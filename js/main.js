'use strict';

const ACCESS_TOKEN = "468fe480b111de779e9af0f2701d98fb35181f07d17ba7dfe0fc861029138475";

function hideLoader() {
	// body...
}

function showLoader() {
	// body...
}

function getShots() {


	var url = 'https://api.dribbble.com/v1/shots/?page=1&access_token=';
	$.getJSON(url + ACCESS_TOKEN, function(data) {
		
		$.each(data, function(index, shot) {

			console.log(index);
			console.log(shot);

		var thumbnail = '<div class="thumbnail">' +
							'<img src="'+ shot.images.normal + '" alt="">' +
							'<div class="caption">' +
								'<h3>'+ shot.title + '</h3>' +
								'<p>'+ shot.description + '</p>' +
								'<p><a href="' + shot.html_url +'" class="btn btn-primary">Show on Dribble</a></p>' +
							'</div>' +
						'</div>';

		$('#shots').append($(thumbnail)); // Pridejimas i #shots
		
		});

	});

}


$(document).ready(function() {

	getShots();


});